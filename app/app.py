from typing import Collection
import requests
import json
import time
from pymongo import MongoClient

mc = MongoClient('localhost', 27017)
db = mc['lenovo']
Collection = db['monitors']

url = "https://www.lenovo.com/co/es/search/facet/query/v3?ch=1471061937&categoryCode=Monitors&categories=ACCESSORY&page=0&pageSize=20&sort=sortBy&currency=COP&cmsFacets=facet_Price,facet_Type,facet_ScreenSize,facet_Group"

payload = {}
headers = {
    'authority': 'www.lenovo.com',
    'pragma': 'no-cache',
    'cache-control': 'no-cache',
    'sec-ch-ua': '"Google Chrome";v="95", "Chromium";v="95", ";Not A Brand";v="99"',
    'accept': '*/*',
    'content-type': 'application/json;charset=utf-8',
    'x-requested-with': 'XMLHttpRequest',
    'sec-ch-ua-mobile': '?0',
    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.69 Safari/537.36',
    'sec-ch-ua-platform': '"Linux"',
    'sec-fetch-site': 'same-origin',
    'sec-fetch-mode': 'cors',
    'sec-fetch-dest': 'empty',
    'referer': 'https://www.lenovo.com/co/es/dc/monitors?sort=sortBy&currentResultsLayoutType=grid',
    'accept-language': 'es,en-US;q=0.9,en;q=0.8',
    'cookie': 'LISAModel={"mcvisid":"50813825703550467732793538225024243313","rawscore":-1,"score":"low"}; initUtilityDock=true; Q8395popupfired; initUtilityDock=true; at_check=true; AMCVS_F6171253512D2B8C0A490D45%40AdobeOrg=1; s_ecid=MCMID%7C50813825703550467732793538225024243313; split=control; split10=test; MSART_coes=PC%20%26%20TABLETS%7C%2Fco%2Fes%2Fpc; Q3234RecBpopupPageViewValue_Geo_CO_Lan_ES=1; _fbp=fb.1.1625170539943.1137893390; usi_id=rftje4_1625170541; QuantumMetricUserID=cd705e12a33a1aa0f3d8a868b08e378b; xdVisitorId=10164AcGb5ONUyxFK5HPtrzZfqnH1eCYLJ3pcP6ECAFwRm448EB; atgRecVisitorId=10164AcGb5ONUyxFK5HPtrzZfqnH1eCYLJ3pcP6ECAFwRm448EB; s_cc=true; aam_uuid=51029568668118088112779088801130177327; LSESSION=B522A21C-A6A8-4EF5-B33D-9DE8038DFAA7; BVBRANDID=6361cb48-3693-4546-b468-64a04a187a20; cust-info=eyJkaXNwbGF5VWlkIjoiYmVxdWlyb3p0QHVuYWwuZWR1LmNvIiwibmFtZSI6IkVybmVzdG8gUXVpcm96IiwiY3VzdG9tZXJJRCI6IjIxZDZjMjI5LTQ1YzktNDM3OC1hMWY4LTczMmRiNjllY2JlMCJ9; s_eng_rfd=0.00; has_consent_cookie=; ku1-sid=gWjoR37PDhn1AAJqD3S0m; ku1-vid=67288b08-6ccd-dc6a-8bbf-490eeb3d8509; IR_gbd=lenovo.com; _mibhv=anon-1626055317791-7191982368_4876; _pin_unauth=dWlkPU9XRmxZemszWVRRdE4yWTFOQzAwTVdNeUxXRmpOREF0T1RNeU9HTXpNMkkxT0dGaQ; ki_r=aHR0cHM6Ly93d3cuZ29vZ2xlLmNvbS8%3D; MSART_coescoedu=PC%20%26%20TABLETS%7C%2Fco%2Fes%2Fcoedu%2Fpc; atgRecSessionId=ZNg295VTvdxoodUZ86fxZ4vg9AgRIYA5_2a7BUlSQD82sAGbiLU_u00211553693814u0021456350200; inside-pcwfid=8; p3094257258_done=1; _gcl_au=1.1.1616916765.1634699281; gclid=Cj0KCQjwlOmLBhCHARIsAGiJg7lP2Ch7AcsyZATgh5QsjiHmSiRWlMTTbeuiz8nuSPzyLiMYJFwQuZgaAgXVEALw_wcB; _gcl_aw=GCL.1635426009.Cj0KCQjwlOmLBhCHARIsAGiJg7lP2Ch7AcsyZATgh5QsjiHmSiRWlMTTbeuiz8nuSPzyLiMYJFwQuZgaAgXVEALw_wcB; s_eVar57=%3B82A200BALM; acceleratorSecureGUID=3a71566a2255124b8fc6581611ec3e794ae58e8e; MSART=PC%20%26%20TABLETS%7C%2Fco%2Fes%2Fpc; leid=1.9Rhc4inPDZs; fsid=3; BVImplmain_site=8923; s_tslv=1635521904939; s_vnc365=1667057904944%26vn%3D2; s_tot_rfd=1.08; ftv_for_usen=%252Fpc%252F; IR_3808=1635521918878%7C191038%7C1635521918878%7C%7C; _uetvid=23469be0e2b511eb9d13d3aa402fc925; ki_t=1626055318297%3B1635521919154%3B1635521919154%3B2%3B3; IR_PI=23d93f95-e2b5-11eb-940d-693b4cf5a826%7C1635608318878; _derived_epik=dj0yJnU9SmlZR0RncnQxTUxTLTFWMnoyUjdXOXh0TVhMY1RZOXUmbj1pVzdHRmVTMGNFNVBDZTRuVHJZN1hnJm09MSZ0PUFBQUFBR0Y4Rlg4JnJtPTEmcnQ9QUFBQUFHRjhGWDg; _mkto_trk=id:026-ENO-001&token:_mch-lenovo.com-1625170541992-70481; aam_sc=aamsc%3D7139268%7C8145378; s_fid=589E2E9F2D65AA55-0AA230FFAF858082; JSESSIONID=B2EEB8D059E84395DC82BBD0531BE1A4.app21; s_sq=%5B%5BB%5D%5D; AKA_A2=A; _abck=B56CF1D4DCD04D290C0ABB75581C800F~0~YAAQTbctFweS2Ah9AQAAzdMBFwbGAuaxwmHPDV/th8ocaz3rD3LramJbwXSPWVai5On+T8U8pBuUvzUFXFUEd4NPIU9lim83RHkdrXcsaV1MlyADW+cvfeh259kiZMa5xWj/Pd0JcFQIlm55i7e6I1afuMGzE06k6D3II3naY/h0GCJxwN4woAEgbxdCc3j1NOHSuXDtfcHXgCXDqTQICEYh/e+DlPiEiTmeNvdzni8b1zrlla7FhzQrbTs1lm1rzE5PcjofS7Crm000hD60fskYIGUamHNS6jbAUuGCrQR1+Om2w4wk1Z4YjQkP2Xzv6zDCY7+lnDunBL23vDxKg675n8W9+eV3BG1N/kxYoMD1Z/GdZ0alceQPqsiESZur8EG3iJTW630ylkVZWajCz4ZOhClSL40=~-1~-1~-1; bm_sz=51BA6BB1771219D204397A8B5094826C~YAAQTbctFwmS2Ah9AQAAzdMBFw2Tf3XeWtR/FQ9KLBUtl6G+BhfH22leFKEDT4d0wmxEKvhYIwDal36eZwd56XR0sKF/iNvplfj42snoJnkcOB2A7uLOckbBtSdb7JOHR+zyyy2HoUck6TJw/lozUvZSgOgo6SVBjGEgfJ3pUuHeHVi0Gb0N+TnyeIkSEKBNeyQGIEfowk1MDSHKgEGcunQVQ+j/jfRNAy7+dVvPhmi1zlhDsmK0ZjThN+QzpnLLODV7eqG0n9Gbfee3J75D1VH7PMHw7VCsNKPgrNXa2qvHsDs=~4404806~3555651; AMCV_F6171253512D2B8C0A490D45%40AdobeOrg=-1124106680%7CMCIDTS%7C18944%7CMCMID%7C50813825703550467732793538225024243313%7CMCAAMLH-1637373337%7C4%7CMCAAMB-1637373337%7CRKhpRz8krg2tLO6pguXWp5olkAcUniQYPHaMWWgdJ3xzPWQmdj0y%7CMCOPTOUT-1636775737s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C5.2.0%7CMCCIDH%7C-378819682; bm_mi=158CF8FB8001A7563F1E1274B073D040~wwgaZf6KD9/u/VwDxJn2ya6VhmGKdyvt3x0c4+FyzIFOxAJxzKKxtk/CmG4lWiqcR7OuLxIIcIOKwRt8EIMnZxb2KYJs6MKQXFl0k/3kBpob41xtGjry2Eh54Hv3yXj4uG3iQBEgP3Am11Y7jlRyzeVDUgEpMf48sbAdbfFJyYUrFknWYFaVOPkobnG9r5AmE2rj7XlSMUUtstQknm6RvB84zAQ/Bi4XyWRgRYIyYXY4QRRNpmORA2R6PlwixbK3fqU2VfMELw0da833S7ussUztR153eLgygt2z+dgsu3Q=; s_dfa=lenovocopub%2Clenovoglobal; ak_bmsc=2B6D5F4A99455268A34D48DA76E97C4E~000000000000000000000000000000~YAAQTbctF4KS2Ah9AQAAe+cBFw2bO+Qt1U4ssBmaJhBiIs7Bv9I/PZUbaHFHAUCrm7KJH1mPVCcFR8efX3Mmonorezo23Vnx71rw9N4VAVXgHk7VfNKC2/E/qPQidyYk7jqfHxRYTWKxZQPEJBKbRjl69RnGt641wv/3Yo9UBAiRILUppY2CqdHWCYjNmxBUOUY6fkWmfnRrYK3fqpg+5ce36erHWxZQt+kfN9mUCINjidnvfNQurcE4Ab+CtbYZTCsuRbER0WLDJ5+7A1kyxt2JE3tZ5yI13PVx3cYiA2ecRoMtUEEhc7LAHiQZKwQdHxHmp3C46PcYPEV0nN93UKWtxmV3FuB4+gqWQzV3fa/JtmRCymPnawfTtbgF75GwtU7G+NnKDocONCENZZqKoF60NVD0HVWgibcpb4zEqAGH2WhA2xUKMTla/AMoD/zZLE38F2pCfNA+d1dBBYEbp1AuXGrai2q5e9YjtPyjhp4n4GdJv/ixcF6+Hhe6l1WyurM2G6DJXv2T0w==; QuantumMetricSessionID=6e4cdd66e5a55fc9040f278569119984; mbox=PC#5abf92592e7e4f91b464daf739855df2.34_0#1688415340|session#cbfe9f9af0f3494aa5e408f41d067b95#1636770404; Q3234RecBpopupTotalPageViewValue_Geo_CO_Lan_ES=11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111; s_gpv=co_es%3Adc%3Amonitors; utag_main=v_id:017a63b6727700004a6ebaec8b0b03068014f06000bd0$_sn:89$_se:3$_ss:0$_st:1636770343412$dc_visit:88$ses_id:1636768536892%3Bexp-session$_pn:3%3Bexp-session$dc_event:2%3Bexp-session$dc_region:us-east-1%3Bexp-session; aamtest1=seg%3Dabc%2Cseg%3Ddef%2Cseg%3Dghi; inside-us3=3704620-c6d5d79c5331554e5d18fcde8cd47658eb1d036eaf25d639cbe269289a1bcf13-0-0; s_tp=5288; s_ppv=co_es%253Adc%253Amonitors%2C15%2C15%2C811.6666717529297; MSESSIONID=CE54B5023D8DD9A383CC347BEEB23BEE; bm_sv=93617572C81299F174794598BB3616EB~NuNBfFhscS9Dc5L+j/khSDVfLtTdzMOnUSNQzpD2QCUyppIgLef2PGGqauWbuCn7KJcWG2Tr/QVjm8V69dDviZTJ27+L8YT7V2RCYbVtz0IXoNNUkdJ5ukhBL7ieR5snuh2LSB+qPDa6MxrQ6aenP6koqZNas5RYBBSbogMD+hk=; akavpau_WaitingRoomController=1636769142~id=f149b0c51e64ad91aef16e94201a24f4; RT="z=1&dm=lenovo.com&si=b4d80758-c29c-4f2d-b072-b5ee5f908809&ss=kvx5q52i&sl=3&tt=4sz&bcn=%2F%2F17de4c1c.akstat.io%2F"; LSESSION=B522A21C-A6A8-4EF5-B33D-9DE8038DFAA7; _abck=B56CF1D4DCD04D290C0ABB75581C800F~-1~YAAQR6tLaARF3dp8AQAAhkgSFwYLcjJS2cr99QS3Cn3bsIc8DmU02m/qRwWk5v6DV0om+NLBjAP0dCyZwtNCgyMsPFnayyVxQK/2Sl4aFGcpY3lA6xZbMDHcsG7z/KnM4Hy2Dn2zHUj0cljTSpkYdRxbmrxLl856cidBnRSg5bH3ecTpzvbcMK1eMFPMVMWRaBaQOP5soF4VcuuL+i6szgpYPU4GvKZsrs9xkJW296+b5iuyySjiUXpc7ltPgOlS0DgBETyCVRmnr7R0kXwMUG5E34yIlD2r5d3+hfmXANY7ih71BjrqXJIB+C1UAT0GehBaRLmtRPbVnZpUyXWCzlLB/Pf2dd2KeXISv/huM3d0H5K96EcbsJnfSUfOZw+LlwVuyO5St1+5xBNLsDW2wCVuwLVFDaI=~0~-1~-1; bm_sv=93617572C81299F174794598BB3616EB~NuNBfFhscS9Dc5L+j/khSDVfLtTdzMOnUSNQzpD2QCUyppIgLef2PGGqauWbuCn7KJcWG2Tr/QVjm8V69dDviZTJ27+L8YT7V2RCYbVtz0IFW6AowBKPN4OOKXyEfNcQu9ewkIX/VSJngpDz2p8B0pwq06RXAnBAlruaxsEuZm0=; MSESSIONID=F9E84F5C307DA578B76809C57496605D; akavpau_WaitingRoomController=1636769913~id=f3a7f598d9fff8d5cc093b973d0f0e94'
}

def saveMonitorToDB(result):
    code = result['code']
    del result['code']
    result['_id'] = code
    if Collection.count_documents( {'_id' : result['_id']}, limit=1):
        print("Don't allow to load because register already exist")
        return
    try:
        Collection.insert_one(result)
        print("Loaded successfully!")
    except:
        print("Data not loaded")

def scrollForAllMonitorsList(response):
    json_response = json.loads(response.text)
    if json_response['totalResults'] == 0:
        print("Don't have more monitors")
        return
    results = json_response['results']
    for result in results:
        name = result['name']
        print(name)
        saveMonitorToDB(result)


attempt = 1
while attempt < 5:
    try:
        response = requests.request("GET", url, headers=headers, data=payload)
        scrollForAllMonitorsList(response)
        break
    except requests.exceptions.RequestException as err:
        attempt += 1
        if err == requests.exceptions.HTTPError:
            print ("Http Error:",err)
        elif err == requests.exceptions.ConnectionError:
            print ("Error Connecting:",err)
        elif err == requests.exceptions.Timeout:
            print ("Timeout Error:",err)
        else:
            print ("Other Error:",err)
        print(response.status_code)
        time.sleep(1)
        
